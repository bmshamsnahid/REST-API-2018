let mongoose = require ('mongoose');
mongoose.connect ('mongodb://localhost:27017/mongo-exercises', { useNewUrlParser: true })
    .then ( () => console.log ('Database connected successfully.') )
    .catch ( (error) => console.error (`Error in database connection: ${error}`));

const courseSchema = new mongoose.Schema ({
    tags: [ String ],
    date: { type: Date, default: Date.now },
    name: { type: String },
    author: { type: String },
    isPublished: { type: Boolean },
    price: { type: Number }
});

const Course = mongoose.model ('Course', courseSchema);

const getPublishedFrontendAndBackendCourses = async () => {
    const courses = await Course
        .find ({ isPublished: true, tags: { $in: ['frontend', 'backend']} })
        .sort ({ price: -1 })
        .select ({ name: 1, author: 1 });
    console.log (courses);
};

getPublishedFrontendAndBackendCourses();