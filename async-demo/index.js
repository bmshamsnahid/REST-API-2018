let getUser = (id) => {
    return new Promise ((resolve, reject) => {
        setTimeout(() => {
            console.log('Reading a user from database...');
            return resolve ({ id: id, gitHubUsername: 'bmshamsnahid' });
        }, 2000);
    });
};

let getRepositories = (username) => {
    return new Promise ((resolve, reject) => {
        setTimeout(() => {
            console.log('Getting repos ...');
            // resolve (['repo1', 'repo2', 'repo3']);
            reject (new Error ('Could not get repos'));
        }, 2000);
    });
};

let getCommits = (repo) => {
    return new Promise ((resolve, reject) => {
        setTimeout(() => {
            console.log ('Getting commits ...');
            resolve (['com1', 'com2', 'com3']);
        }, 2000);
    });
};


console.log ('Before');

let displayCommits = async () => {
    try {
        const user = await getUser (1);
        const repo = await getRepositories (user.username);
        const commits = await getCommits (repo);
        console.log (commits);
    } catch (err) {
        console.log('Error: ' + err.message);
    }
};
displayCommits();
console.log('After');