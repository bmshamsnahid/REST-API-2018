const mongoose = require('mongoose');
const Joi = require('joi');

const Customer = mongoose.model('Customer', new mongoose.Schema({
    isGold: { type: Boolean },
    name: { type: String },
    number: { type: String }
}));

let validateCustomer = (customer) => {
    const schema = {
        isGold: Joi.required(),
        name: Joi.string().required(),
        number: Joi.string().min(9).required()
    };

    return Joi.validate(customer, schema);
};

module.exports = {
    Customer,
    validateCustomer
};