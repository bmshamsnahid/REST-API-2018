const { Customer, validateCustomer } = require('../models/customer');
const express = require ('express');
const router = express.Router();


router.get ('/', async (req, res, next) => {
    const customers = await Customer.find();
    res.send (customers);
});

router.get ('/:id', async (req, res, next) => {
    const customer = await Customer.findById(req.params.id);
    res.send (customer);
});

router.post ('/', async (req, res, next) => {
    const { error } = validateCustomer( req.body );
    if ( error ) {
        res.status (400).send (error.details[0].message);
    }
    let customer = new Customer (req.body);
    customer = await customer.save();

    res.send (customer);
});

router.put('/:id', async (req, res, next) => {
    const { error } = validateCustomer( req.body );
    if ( error ) {
        res.status (400).send (error.details[0].message);
    }
    const customer = await Customer.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.send (customer);
});

router.delete('/:id', async (req, res, next) => {
    const customer = await Customer.findByIdAndRemove(req.params.id);
    if (!customer) {
        return res.status (404).send ('Invalid Id');
    }
    return res.send (customer);
});

module.exports = router;

