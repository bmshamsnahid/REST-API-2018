const mongoose = require ('mongoose');

mongoose.connect ('mongodb://localhost:27017/playground', { useNewUrlParser: true })
    .then (() => { console.log ('Connected to mongoDB... ')})
    .catch ( (err) => { console.error (`Could not connect mongoDB database:  ${err}`) });

const courseSchema = new mongoose.Schema ({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        /*match : /pattern/*/
    },
    category: {
      type: String,
      required: true,
      enum: [ 'web', 'mobile', 'network' ],
      lowercase: true,
      // uppercase: true,
      trim : true
    },
    author: String,
    tags: {
        type: Array,
        validate: {
            isAsync: true,
            validator: function (v, callback) {
                setTimeout( () => {
                    const result = v && v.length > 0;
                    callback (result);
                }, 4000);
            },
            message: 'A course should have at least one tag.'
        }
    },
    data: { type: Date, default: Date.now },
    isPublished: Boolean,
    price: {
        type: Number,
        required: function () {
            return this.isPublished;
        },
        min: 10,
        max: 200,
        get: v => Math.round (v),
        set : v => Math.round (v)
    }
});

const Course = mongoose.model ('Course', courseSchema);

const createCourse = async () => {
    const course = new Course ({
        name: 'Angular Course',
        category: 'Web',
        author: 'Mosh',
        tags: [ 'frontend' ],
        isPublished: true,
        price: 15.8
    });

    try {
        const result = await course.save();
        console.log (result);
    } catch (exception) {
        for (fields in exception.errors) {
            console.log (exception.errors [ fields ].message);
        }
    }
};

const getCourses = async () => {
    // eq ( equal )
    //ne ( not equal )
    //gt ( greater than )
    //gte ( greater than or equal )
    //lt ( less than )
    //lte ( less than or equal )
    //in
    // nin ( not in )

    const pageNumber = 2;
    const pageSize = 10;

    const courses = await Course
        .find()
        .skip ( ( pageNumber - 1 ) * pageSize )
        .limit ( pageSize )
        .sort ({ name: 1 })
        .select ({ name: 1, tags: 1 })
        .countDocuments ();
    console.log (courses);
};

const updateCourse = async (id) => {
    const result = await Course.findByIdAndUpdate (id, {
        $set: {
            author: 'Mosh',
            isPublished: false
        }
    }, { new: true });
    console.log (result);
};

const removeCourse = async (id) => {
    const course = await Course.findByIdAndRemove (id);
    console.log ( course );
};

createCourse();
// getCourses();
// updateCourse('5b6d303366cc4622d8a7b5d0');
// removeCourse('5b6d303366cc4622d8a7b5d0');