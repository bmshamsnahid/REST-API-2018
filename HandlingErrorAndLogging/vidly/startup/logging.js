const winston = require('winston');
require('winston-mongodb');
require('express-async-errors');

module.exports = () => {
    winston.add(new winston.transports.Console({ colorize: true, prettyPrint: true }));
    winston.add(new winston.transports.File({ filename: 'logfile.log'}));
    winston.add(new winston.transports.MongoDB({ db: 'mongodb://localhost/vidly'}));

    process.on('uncaughtException', (ex) => {
        winston.error(ex.message, ex);
    });

    process.on('unhandledRejection', (ex) => {
        throw ex;
    });
};