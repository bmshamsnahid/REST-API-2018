const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground', { useNewUrlParser: true })
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema]
}));

async function createCourse(name, authors) {
  const course = new Course({
    name, 
    authors
  }); 
  
  const result = await course.save();
  console.log(result);
}

async function listCourses() { 
  const courses = await Course.find();
  console.log(courses);
}

async function updateAuthor(courseId) {
  const course = await Course.update({_id: courseId}, {
    $unset: {
      'author': ''
    }
  });
}

async function addAuthor(courseId, author) {
  const course = await Course.findById(courseId);
  console.log (course.authors);
  course.authors.push (author);
  const result = await course.save();
  console.log (result);
}

async function removeAuthor(courseId, authorId) {
  const course = await Course.findById(courseId);
  console.log (course.authors);
  const author = course.authors.id(authorId);
  await author.remove();
  const result = await course.save();
  console.log (result.authors);
}

// createCourse('Node Course', [
//   new Author({ name: 'Mosh' }),
//   new Author({ name: 'Smith' })
// ]);
// updateAuthor('5b7e6fd01d7f7c17f14bcc5e');
// addAuthor('5b7e7803c6c2b626064bfa2e', new Author({name: 'Amy'}));
removeAuthor('5b7e7803c6c2b626064bfa2e', '5b7e79dec1e2f028f00fe49d');