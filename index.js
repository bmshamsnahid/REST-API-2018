const express = require('express');
const startupDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');
const app = express();
const Joi = require('joi');
const morgan = require('morgan');
const config = require('config');

const courses = require('./routes/courses');

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(express.static('public'));

if (app.get('env') == 'development') {
    app.use(morgan('tiny'));
    startupDebugger('Morgan Enabled... ');
}

dbDebugger('Connected to the database');

app.use('/api/courses', courses);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`App is listening on port ${port}`));

